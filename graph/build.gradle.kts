plugins {
    java
}

group = "com.sony.dnn.sdsp.convfe.hw"
version = "0.1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation("junit:junit:4.13.2")
}

tasks.test {
    useJUnitPlatform()
    useJUnit()
    maxHeapSize = "1G"
}

