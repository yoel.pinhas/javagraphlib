package com.sony.graph;

import java.util.*;

public class Graph<T extends Comparable<T>> {
    public static <T extends Comparable<T>> Builder builder() {
        return new Builder<T>();
    }

    public static <T extends Comparable<T>> Graph<T> chain(T... ts) {
        var b = Graph.<T>builder();
        if (ts.length == 1)
            b.addNode(ts[0]);
        else if (ts.length > 1) {
            for (int i = 0; i < ts.length - 1; ++i) {
                b.addEdge(ts[i], ts[i + 1]);
            }
        }

        return b.toGraph();
    }

    public static class Builder<T extends Comparable<T>> {
        private final Set<T> nodes;
        private final Set<Edge<T>> edges;

        Builder(Set<T> nodes, Set<Edge<T>> edges) {
            this.nodes = nodes;
            this.edges = edges;
        }

        Builder() {
            this(new LinkedHashSet<>(), new LinkedHashSet<>());
        }

        public Graph<T> toGraph() {
            return new Graph<>(nodes, edges);
        }

        private boolean nodeExist(T n) {
            return nodes.contains(n);
        }

        private boolean nodeMissing(T n) {
            return !nodeExist(n);
        }

        public Builder<T> addNode(T n) {
            nodes.add(n);
            return this;
        }

        private T addNodeWhenMissing(T n) {
            if (nodeMissing(n)) nodes.add(n);
            return n;
        }

        public Builder<T> addEdges(Iterable<Edge<T>> edges) {
            edges.forEach(this::addEdge);
            return this;
        }

        public Builder<T> addEdge(Edge<T> e) {
            addNodeWhenMissing(e.getDst());
            addNodeWhenMissing(e.getSrc());
            edges.add(e);
            return this;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Builder)) return false;
            Builder<?> builder = (Builder<?>) o;
            return Objects.equals(nodes, builder.nodes) && Objects.equals(edges, builder.edges);
        }

        @Override
        public int hashCode() {
            return Objects.hash(nodes, edges);
        }

        public Builder<T> addEdge(T src, T dst) {
            Edge<T> e = new Edge<>(addNodeWhenMissing(src), addNodeWhenMissing(dst));
            edges.add(e);
            return this;
        }

    }

    private final Set<T> nodes;
    private final Set<Edge<T>> edges;

    // do not create instances directly, use #Builder instead
    private Graph(Set<T> nodes, Set<Edge<T>> edges) {
        this.nodes = nodes;
        this.edges = edges;
    }

    public Builder<T> toBuilder() {
        return new Builder<>(nodes, edges);
    }

    public boolean edgeExist(Edge<T> e) {
        return edges.contains(e);
    }

    public boolean nodeExist(T n) {
        return nodes.contains(n);
    }

    public boolean nodeMissing(T n) {
        return !nodeExist(n);
    }

    public Set<Edge<T>> inEdges(T n) {
        var inEdges = new LinkedHashSet<Edge<T>>();
        edges.forEach(e -> {
            if (e.getDst() == n) inEdges.add(e);
        });
        return inEdges;
    }

    public Set<T> inNodes(T n) {
        var parents = new LinkedHashSet<T>();
        edges.forEach(e -> {
            if (e.getDst() == n) parents.add(e.getSrc());
        });
        return parents;
    }

    public Set<Edge<T>> outEdges(T n) {
        var outEdges = new LinkedHashSet<Edge<T>>();

        edges.forEach(e -> {
            if (e.getSrc() == n) outEdges.add(e);
        });
        return outEdges;
    }

    public Set<T> outNodes(T n) {
        var children = new LinkedHashSet<T>();
        edges.forEach(e -> {
            if (e.getSrc() == n) children.add(e.getDst());
        });
        return children;
    }

    public int inDegree(T n) {
        return inNodes(n).size();
    }

    public int outDegree(T n) {
        return outNodes(n).size();
    }

    public boolean isSource(T n) {
        return inDegree(n) == 0;
    }

    public boolean isSink(T n) {
        return outDegree(n) == 0;
    }

    public Set<T> sources() {
        var sources = new LinkedHashSet<T>();
        nodes.forEach(n -> {
            if (isSource(n)) sources.add(n);
        });
        return sources;
    }

    public Set<T> sinks() {
        var sinks = new LinkedHashSet<T>();
        nodes.forEach(n -> {
            if (isSink(n)) sinks.add(n);
        });
        return sinks;
    }

    private void requireNodeExist(T n) {
        if (nodeMissing(n)) throw new RuntimeException(n + " is missing from graph");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Graph)) return false;
        Graph<?> graph = (Graph<?>) o;
        return Objects.equals(nodes, graph.nodes) && Objects.equals(edges, graph.edges);
    }

    @Override
    public String toString() {
        return "Graph{" +
                "nodes=" + nodes +
                ", edges=" + edges +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(nodes, edges);
    }

    public Graph<T> explore(T n) {
        requireNodeExist(n);
        // explore all directions, up and down
        var exploredEdges = new HashSet<Edge<T>>();
        var front = new HashSet<T>();
        var visited = new HashSet<T>();
        front.add(n);

        // while front is not empty
        // explore front up/down
        // look at all nodes, filter visited
        // un-visited are the new frontier, anything is now visited
        while (!front.isEmpty()) {
            var spreadingEdges = new LinkedHashSet<Edge<T>>();
            front.forEach(_n -> {
                spreadingEdges.addAll(inEdges(_n));
                spreadingEdges.addAll(outEdges(_n));
            });
            exploredEdges.addAll(spreadingEdges);
            var all = new HashSet<T>();
            spreadingEdges.forEach(e -> all.addAll(e.toNodesSet()));
            visited.addAll(front);
            all.removeAll(visited);
            front = new HashSet<>(all);
            visited.addAll(all);
        }
        return builder().addNode(n).addEdges(exploredEdges).toGraph();
    }

    /**
     * for the graph [a->b; b->c] disjoint will return Set([a->b, b->c)])
     * for the graph [a->b; x->y] disjoint will return Set([a->b], [x->y])
     * @return a set of connected graphs disjointed from one another. if this is connected it would be returned by itself
     */
    public Set<Graph<T>> disjoint() {
        Set<Graph<T>> disjoints = new HashSet<>();
        sources().forEach(source -> disjoints.add(explore(source)));
        return disjoints;
    }

    /**
     * IMPLEMENT ME
     * @return true if this graph doesn't contains cycles, false o.w.
     * a cycle is a path from a node in the graph that ends in thw same node
     **/
    public boolean isDag() {
        return true;
    }

    /**
     * IMPLEMENT ME
     * @ return the int level of each node
     * instructions:
     * 1. no need to solve the case for cyclic graphs
     * 2. for any node, level(node) > level(inNode(node))
     * 3. for any node, level(node) < level(outNode(node))
     * 4. provide a deterministic solution, repeated calls to level() yields the same result
     * watch out for graphs where there are multiple ways to reach a node such as
     * (a->b; a->e; b->c; c->d; d->e). in this case a legal leveling might be a:1, b:2, c:3, d:4, e:5
     * in this case illegal leveling might be a:1, e:2 b:2, c:3, d:4.
     * because if e:2 but d is the in node of e and d:4, which violate rules 2 and 3
     */
    public int level(T node) {
        return -1;
    }

}
