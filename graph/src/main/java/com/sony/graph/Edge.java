package com.sony.graph;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * directed edge from src to dst
 *
 * @param <T>
 */
final public class Edge<T> implements Comparable<Edge<T>> {
    private final T src;
    private final T dst;
    private final int weight;

    public Edge(T src, T dst) {
        this(src, dst, 1);
    }

    public Edge(T src, T dst, int weight) {
        this.src = src;
        this.dst = dst;
        this.weight = weight;
    }

    public T getSrc() {
        return src;
    }

    public T getDst() {
        return dst;
    }

    public int getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return src + "->" + dst;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Edge)) return false;
        Edge<?> edge = (Edge<?>) o;
        return Objects.equals(getSrc(), edge.getSrc()) && Objects.equals(getDst(), edge.getDst()) && getWeight() == edge.getWeight();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSrc(), getDst(), getWeight());
    }

    public Set<T> toNodesSet() {
        var s = new HashSet<T>();
        s.add(src);
        s.add(dst);
        return s;
    }

    @Override
    public int compareTo(Edge<T> edge) {
        return hashCode() - edge.hashCode();
    }
}

