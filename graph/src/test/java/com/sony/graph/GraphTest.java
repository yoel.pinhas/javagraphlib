package com.sony.graph;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertTrue;

public class GraphTest {

    // helper method for lower boilerplate code
    private <T> Set<T> newSet(T ... vs) {
        var l = new LinkedHashSet<T>();
        for (var v: vs) { l.add(v); }
        return l;
    }

    private void print(String s) { System.out.println(s); }

    private <T> void assertEmptySet(String msg, Set<T> set) {
        assertTrue(msg + " expecting set to be empty, instead '" + set + "'", set.isEmpty());
    }

    private <T> void assertSetContent(String msg, Set<T> set, T ... ts) {
        assertTrue(msg + " expecting '" + set + "' to be equal to '"
                + Arrays.toString(ts)
                + "'", set.equals(newSet(ts)));
    }

    private <T> void assertSetContent(String msg, Set<T> set, Set<T> actual) {
        assertTrue(msg + " expecting '" + set + "' to be equal to '"
                + actual
                + "'", set.equals(actual));
    }

    // very simple graphs helper for tests
    interface G {
        String n0 = "N0";
        String n1 = "N1";
        String n2 = "N2";
        String n3 = "N3";
        String n4 = "N4";
        Graph<String> g0 = Graph.chain();
        Graph<String> g1 = Graph.chain(n0);
        Graph<String> g2 = Graph.chain(n0, n1);
        Graph<String> g3 = Graph.chain(n0, n1, n2);
        Graph<String> g4 = Graph.chain(n0, n1, n2, n3);
        Graph<String> g5 = Graph.chain(n0, n1, n2, n3, n4);

        Graph<String> diamond = Graph.builder()
                .addEdge("A", "X")
                .addEdge("A", "Y")
                .addEdge("A", "Z")
                .addEdge("X", "B")
                .addEdge("Y", "B")
                .addEdge("Z", "B")
                .toGraph();
    }

    @Test
    public void nodeExist() {
        String node = "A";
        Graph<String> g = Graph.builder().addNode(node).toGraph();
        assertTrue(g.nodeExist(node));
    }

    @Test
    public void edgeExist() {
        assertTrue(G.g2.nodeExist(G.n0));
        assertTrue(G.g2.nodeExist(G.n1));
        assertTrue(G.g2.edgeExist(new Edge(G.n0, G.n1)));
    }

    @Test
    public void sources() {
        assertEmptySet("sources", G.g0.sources());
        assertSetContent("sources", G.g2.sources(), G.n0);
    }

    @Test
    public void sinks() {
        assertEmptySet("sinks", G.g0.sinks());
        assertSetContent("sinks", G.g2.sinks(), G.n1);
    }

    @Test
    public void inNodes() {
        assertEmptySet("inNodes", G.g2.inNodes(G.n0));
        assertSetContent("inNodes", G.g2.inNodes(G.n1), G.n0);
    }

    @Test
    public void outNodes() {
        assertEmptySet("outNodes", G.g2.outNodes(G.n1));
        assertSetContent("outNodes", G.g2.outNodes(G.n0), G.n1);
    }

    @Test
    public void outNodesDeterministic() {
        var source = G.diamond.sources().iterator().next();
        var outNodes = G.diamond.outNodes(source);
        for (int i=0; i < 100; ++i) {
            assertSetContent("outNodes", G.diamond.outNodes(source), outNodes);
        }
    }

    @Test
    public void explore() {
        var g2 = G.g5.explore(G.n2);
        var g3 = G.g5.explore(G.n3);
        assertTrue(g2.equals(g3));
    }

    @Test
    public void disjoint() {
        Graph<String> g = Graph.builder()
                .addEdge("1", "2")
                .addEdge("2", "3")
                .addEdge("2", "4")
                .addEdge("0", "2")
                .addEdge("a", "b")
                .addEdge("c", "b")
                .addNode("X")
                .toGraph();
        var gs = g.disjoint();
        assertTrue(gs.size() == 3);
        // also, assert that once we assemble all the disjointed parts we get g
        // also, assert that all disjoint graphs diff to one another is empty
    }
}